---
title: Créer un flyer avec Inkscape
theme: white
css: theme/fullstack.css
---

# <small>Créer un flyer avec </small><br>Inkscape

## Atelier du PIC

### 14 mai 2022

---

# C'est quoi Inkscape ?

----

* Logiciel libre de bureau
* Dessin vectoriel
* Pour GNU/Linux, Windows ou MacOS

![Logo Inkscape](https://media.inkscape.org/static/images/inkscape-logo.svg)

----

## Dessin vectoriel ?!

Deux types d'images

----

### Image Bitmap

(ou raster)

* Matrice de pixels
* Information sur la couleur de chaque pixel
* Formats : `.png`, `.jpg`, `.tiff`

![](./images/pixels-from-garonne-gregory-tonon-CC-By-SA.png)

----

### Image Vectorielle

* Description en SVG (schéma XML)
* Formes avec contour et couleur, dégradés, objets textes, groupes d'objets, …
* Formats : `.svg`, `.svgz`

----

## Documentation

Pas mal de contenu inspiré du
[Manuel libre sur Inkscape](https://flossmanualsfr.net/initiation-inkscape/loutil-de-selection/), édité par [Floss Manuals](https://www.flossmanualsfr.net/) sous licence GPLv2

----

## Créer un document

* Ouvrir Inkscape

Format par défaut : A4

----

## Interface

![Interface](./images/interface-inkscape092-haut.png)

----

### Interface (2)

![Interface](./images/interface-inkscape092-bas.png)

---

# Premiers objets vectoriels

----

## Premier objet

* Créer un rectangle ![rectangle](./images/outil-rectangle.png)
* Outil sélection ![sélection](./images/outil-selection.png)
  - Déplacer
  - Transformer

----

## Remplissage et contour

* changer la couleur
* modifier le contour

![remplissage](./images/inkscape-remplissage.png) ![contours](./images/inkscape-contours.png)

----

## Créer des formes géométriques

![Formes géométriques](/images/formes-geometriques.png)

----

### Outils

* Rectangle ![rectangle](./images/outil-rectangle.png)
* Cercle, ellipse, arc de cercle ![ellipse](./images/outil-ellipse.png)
* Étoile et polygones ![etoile](./images/outil-etoile.png)
* Spirale ![spirale](./images/outil-spirale.png)

----

## Les unités

* `mm`, `cm`, `in` : pour l'impression
* `pt`, impression, surtout taille de polices
* `px`, affichage écrans

----

## Le texte

* écrire un texte
  - directement
  - dans un cadre
* modifier taille
* modifier le style

----

## Créer des formes libres

![Forme libre](./images/formes-libres.png)

----

### Outils

* Crayon ![Crayon](./images/outil-crayon.png)
* Courbes de Béziers ![Courbe](./images/outil-bezier.png)
* Tracé calligraphique ![Calligraphie](./images/outil-crayon.png)

----

## Modifier les formes

* Édition des nœuds
* Sculpter les formes
* Effacer des chemins
* Outil remplissage

---

# Concevoir un flyer

----

## Questions à se poser

* Quel message ?
* Pour qui ?
* Objectif du flyer ?
* Durée de vie ?

----

## Format

Les plus courants

* A5
* Carte postale → A6
* Tryptique → A4 plié en 3

----

## Contenu

* Logo, illustration, couleurs de l'asso
* Flyer pérenne : vos activités
* Pour un événement : date et lieu
* Contact, site web

----

## Conseils pour la forme

* Couleurs : 3 maxi
* Typographies : 2 polices maxi
  * Éviter le souligné, peu lisible
* Message : clair et concis
* Images : résolution de 300dpi
* Disposition : aérer le contenu

----

## Faites tester le flyer !

Si possible par quelqu'un qui ne connait pas votre association.

----

## Des exemples

----

<img src="./exemples/contribatelier.png" height="600" />

----

<img src="./exemples/flyer-pic-recto.png" height="600" />

----

<img src="./exemples/toulibre-recto.png" height="600" />

---

# Créer mon flyer

----

## Commencer

- Propriétés du document
  - Dimensions A5
  - Afficher des grilles

→ Aide pour magnétiser les objets

----

### Espace de dessin

- Dessiner un carré → Objets en guides

→ Définit des marges

----

### Fonds

Deux possibilités

- Propriétés du document → couleur de fond
- Dessiner un carré de couleur

----

### Utiliser les calques

- Un calque pour le fond
- Un autre pour le texte
- Un troisième pour les éléments modifiables dans le temps (optionnel)

Évite de déplacer des éléments d'autres calques

----

## Textes

- écrire les textes : titre de l'événement, date, lieu
- modifier taille, couleur des textes
- écrire texte plus long, encadré pour le résumé
- modifier le style

----

## Organiser les éléments

- aligner les éléments
- dupliquer les éléments

----

## Créer des formes complexes

Exemple d'un nuage : fusion de plusieurs cercles de couleur

- fusionner, exclure
- modifier les nœuds

----

## Texte autour d'une forme


---
# Penpot

----

## Fonctionnalités de Penpot

Outil de design en ligne, basé sur un logiciel libre

- dessin vectoriel, SVG natif
- formes, textes, courbes, import d'images, polices nombreuses
- travail collaboratif en équipe possible
- partage de librairies de couleurs, polices, graphiques
- gestion de composants
- export plusieurs formats et échelles

----

### Dessiner dans Penpot

- notion d'artwork
- courbes, textes
- alignement des objets
- grouper

----

### Partager un projet

- partage par lien
- gestion dans une équipe

----

### Utiliser une librairie partagée

Créons un projet "charte graphique"

- Importer le logo
- Importer 2 ou 3 photos
- Ajouter des couleurs
- Partager cette librairie

Puis dans notre premier projet, ajouter cette librairie

---

# Merci !

À lire : le [manuel d'initiation à Inkscape](https://flossmanualsfr.net/initiation-inkscape/) chez Floss Manual

----

## Crédits

Tutoriel sous licence [CC-0](https://creativecommons.org/publicdomain/zero/1.0/deed.fr)

Sauf:

- image pixellisée issue de ["Toulouse Plages"](http://www.flickr.com/photos/eriatarka31/7624636092) par *Gregory Tonon*, **CC-By-SA**
- pictos outils et interface issue de [Initiation Inkscape](https://flossmanualsfr.net/initiation-inkscape/) en GPLv2
- exemples de flyers Contribatelier par Marnic (et image de David Revoy), le PIC au Pic, et Toulibre à Toulibre

---

# Ressources libres

----

## Icônes

- https://www.opensymbols.org/
- https://www.svgrepo.com/
- https://icons.getbootstrap.com/
- https://openmoji.org/
- https://cocomaterial.com/
- https://icofont.com/
- https://fontello.com/

----

## Illustrations

- https://openmoji.org/
- https://doodleipsum.com/
- https://undraw.co/illustrations
- https://www.manypixels.co/gallery

----

## Photos

- https://cc0.photo/
- https://www.pexels.com/
