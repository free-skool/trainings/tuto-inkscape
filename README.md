# Créer un flyer avec Inkscape

Créer un flyer pour son association, pour un événement. Organiser les informations au mieux dans la page, avec l’aide de Inkscape, logiciel libre de dessin vectoriel. Découvrir les nombreuses possibilités de Inkscape au travers de la création d'un flyer.

## Générer le support

La source de ce support est en markdown, [reveal-md](https://github.com/webpro/reveal-md) permet de générer un support en `html` ou en `pdf`

### En html

```bash
reveal-md  flyer-inkscape.md -w --theme solarized
```

### En pdf

```bash
reveal-md --print flyer-inkscape.md --theme solarized
```
